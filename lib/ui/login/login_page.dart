import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/database/user_database.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  bool _isNull = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          print("state is $state");
          if (state is AuthBlocLoggedInState) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
            );
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                _isNull
                    ? Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Center(
                              child: Text(
                                "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.red),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      )
                    : Container(),
                CustomButton(
                  text: 'Login',
                  onPressed: handleLogin,
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              if (val.length == 0) {
                return "Form tidak boleh kosong,mohon cek kembali data yang anda inputkan";
              } else {
                final pattern =
                    new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                return pattern.hasMatch(val)
                    ? null
                    : 'Masukkan e-mail yang valid';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              if (val.length == 0) {
                return 'this field is require';
              } else {
                return null;
              }
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          BlocProvider.of<AuthBlocCubit>(context).register_user();
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    print("EMAIL $_email");
    print("_password $_password");
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      setState(() {
        _isNull = false;
      });
      DatabaseProvider dbProvider = DatabaseProvider();
      var result = await dbProvider.checkLogin(_email, _password);
      if (result) {
        AuthBlocCubit authBlocCubit = AuthBlocCubit();
        User user = User(
          email: _email,
          password: _password,
        );
        authBlocCubit.login_user(user);
        Fluttertoast.showToast(msg: 'Login Berhasil');
        BlocProvider.of<AuthBlocCubit>(context).login_user(user);
      } else {
        Fluttertoast.showToast(
            msg: 'Login gagal, periksa kembali inputan anda');
      }
    } else {
      if (_email == null && _password == null) {
        setState(() {
          _isNull = true;
        });
      } else if (_email != null && _password == null) {
        if (_email.isEmpty) {
          setState(() {
            _isNull = true;
          });
        } else {
          setState(() {
            _isNull = false;
          });
        }
      } else if (_email == null && _password != null) {
        if (_password.isEmpty) {
          setState(() {
            _isNull = true;
          });
        } else {
          setState(() {
            _isNull = false;
          });
        }
      } else {
        if (_password.isEmpty && _email.isEmpty) {
         
        } else {
          setState(() {
            _isNull = false;
          });
        }
      }
    }
  }
}
