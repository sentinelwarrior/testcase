import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:path/path.dart';

class HomeBlocDetailScreen extends StatelessWidget {
  final Data data;

  const HomeBlocDetailScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            iconTheme: IconThemeData(color: Colors.transparent),
            title: Text(
              "Detil Movie",
              style: TextStyle(color: Colors.black),
            ),
          ),
          body: Container(
            width: size.width,
            height: size.height,
            color: Colors.white,
            child: movieItemWidget(context, data),
          ),
        ),
        onWillPop: () async {
          BlocProvider.of<HomeBlocCubit>(context).fetching_data();
          return false;
        });
  }

  Widget movieItemWidget(BuildContext context, Data data) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Image.network(
              data.i.imageUrl,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Container(
              width: size.width,
              child: Text(
                data.l + " (" + data.year.toString() + ")",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          
          Container(
            width: size.width,
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              data.q + " " + data.yr,
              style: TextStyle(
                color: Color(0xFF1B1B1B),
                fontSize: 14,
              ),
            ),
          ),
          SizedBox(
            height: 26,
          ),
          Container(
            width: size.width,
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              "List Series",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: size.width,
            height: 250,
            // color: Colors.red,
            child: ListView.builder(
              itemCount: data.series.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return listItemSeriesWidget(context, data.series[index]);
              },
            ),
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  Widget listItemSeriesWidget(BuildContext context, Series data) {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Image.network(
            data.i.imageUrl,
            // width: MediaQuery.of(context).size.width - 50,
            // scale: 3,
            // height: 180,
            width: 200,
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              width: 200,
              child: Center(
                child: Text(
                  data.l,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
