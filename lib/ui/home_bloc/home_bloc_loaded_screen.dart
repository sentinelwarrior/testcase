import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';

import '../../main.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.transparent),
        title: Text(
          "",
          style: TextStyle(color: Colors.transparent),
        ),
        actions: [
          GestureDetector(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Icon(Icons.logout, color: Colors.black),
            ),
            onTap: () {
              BlocProvider.of<AuthBlocCubit>(context).logout_user();
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (context) => BlocProvider(
                      create: (context) =>
                          AuthBlocCubit()..fetch_history_login(),
                      child: MyHomePageScreen(),
                    ),
                  ),
                  (Route<dynamic> route) => false);
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return movieItemWidget(context, data[index]);
        },
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, Data data) {
    return GestureDetector(
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0))),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 25, right: 25, top: 25),
              child: Image.network(
                data.i.imageUrl,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 25, right: 25, top: 8, bottom: 16),
              child: Text(data.l, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
      onTap: () {
        BlocProvider.of<HomeBlocCubit>(context).fetching_data_detail(data);
      },
    );
  }
}
