class Movie {
  final String title;
  final String poster;
  final String releaseDate;

  Movie({ this.title, this.poster,this.releaseDate });

  Movie.fromJson(Map<String, dynamic> json)
    : title = json['title'],
    releaseDate = json['Release Date'],
      poster = json['poster'];

  Map<String, dynamic> toJson() => {
    'Title': title,
    'Release Date': releaseDate,
    'Poster': poster
  };
}